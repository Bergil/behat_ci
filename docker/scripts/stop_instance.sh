#!/bin/bash
set -ex

# Set the name of the PR builder job and path pattern to the folder with docker-compose files.
PROJECT_NAME="SAMPLE"
PR_INSTANCE_BUILDER_JOB_NAME="${PROJECT_NAME}_PR_INSTANCE_BUILDER"
PR_INSTANCE_BUILDER_PROFILE="${PR_INSTANCE_BUILDER_JOB_NAME}${pullRequestId}"
PR_BUILDS_PATH_PATTERN="${JENKINS_HOME}/jobs/${PR_INSTANCE_BUILDER_JOB_NAME}/workspace/${pullRequestId}"
DOCKER_COMPOSE_PR_TEST_INSTANCE="-f ${PR_BUILDS_PATH_PATTERN}/docker/docker-compose.yml -f ${PR_BUILDS_PATH_PATTERN}/docker/docker-compose.test.yml -f ${PR_BUILDS_PATH_PATTERN}/docker/docker-compose.behat.yml"

# Stop containers.
docker-compose -p ${PR_INSTANCE_BUILDER_PROFILE} ${DOCKER_COMPOSE_PR_TEST_INSTANCE} down

# Clear workspace.
sudo rm -rf ${PR_BUILDS_PATH_PATTERN}/
