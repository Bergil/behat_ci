#!/bin/bash

# Project name.
PROJECT_NAME="SAMPLE"

# Name of the site root folder.
SITE_FOLDER="drupal8"

# Default Behat options to be used.
DEFAULT_BEHAT_OPTIONS="--format=pretty --out=std --format=cucumber_json --out=std";

# Profile name for docker compose.
DOCKER_PROFILE="${JOB_NAME}${pullRequestId}"

# Set the name of the instance stopper name.
INSTANCE_STOPPER_JOB_NAME="${PROJECT_NAME}_INSTANCE_STOPPER"

# Define Docker compose files to be used.
DOCKER_COMPOSE_DEFAULT="-f ${WORKSPACE}/docker/docker-compose.yml -f ${WORKSPACE}/docker/docker-compose.override.yml"
DOCKER_COMPOSE_TEST_INSTANCE="-f ${WORKSPACE}/docker/docker-compose.yml -f ${WORKSPACE}/docker/docker-compose.test.yml -f ${WORKSPACE}/docker/docker-compose.behat.yml"

# Folder with source DB dumps. Getting name of the last DB dump. Folder where DB dump must be placed.
FETCH_DB_JOB_NAME="${PROJECT_NAME}_FETCH_DB"
SOURCE_DUMPS_FOLDER="${JENKINS_HOME}/jobs/${FETCH_DB_JOB_NAME}/workspace"
DB_DUMP_NAME=$(ls -t ${SOURCE_DUMPS_FOLDER}/ | head -n1);
DUMP_FOLDER="${WORKSPACE}/${SITE_FOLDER}/db_dump"

# Folder with source Files and folder where files must be placed.
FETCH_FILES_JOB_NAME="${PROJECT_NAME}_FETCH_FILES"
SOURCE_FILES_FOLDER="${JENKINS_HOME}/jobs/${FETCH_FILES_JOB_NAME}/workspace/files"
FILES_FOLDER="${WORKSPACE}/${SITE_FOLDER}/sites/default"

# Site build commands.
SITE_BUILD_COMMAND="cd /var/www/html;
sleep 15;
for i in 1 2 3 4 5 6 7 8 9 10; do drush sql-drop -y && break || sleep 10; done;
gunzip < /var/www/html/db_dump/${DB_DUMP_NAME} | drush sql-cli;
drush rebuild;
drush cim -y;
drush updb -y;
drush rebuild;
"

# Path to a dummyfile.
DUMMYFILE="${WORKSPACE}/docker/dummyfile"

# Folder with files to archive.
ARTIFACTS_FOLDER="${WORKSPACE}/tests/behat/artifacts/screenshots"

# Variable for the docker-compose port command.
DOCKER_COMPOSE_PORT_CMD="docker-compose -p ${DOCKER_PROFILE} ${DOCKER_COMPOSE_TEST_INSTANCE} port"
