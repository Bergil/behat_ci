#!/bin/sh
set -e

# Add usermod command to Alpine.
echo "http://dl-4.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories && apk add --update shadow

if [ -f /tmp/dummyfile ]; then
    echo -e "Mapping UIDs...\n"
    OLDUID=`id -u www-data`
    NEWUID=`stat -c '%u' /tmp/dummyfile`
    usermod -u $NEWUID www-data
    groupmod -g $NEWUID www-data
    find /var/www/html -user $OLDUID -exec chown -h $NEWUID {} \;
fi

chown -R www-data:www-data /var/www/html
echo -e "UIDs Changed...\n"

docker-entrypoint.sh

exec "$@"
