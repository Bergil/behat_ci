#!/bin/bash

docker-compose -f ./docker/docker-compose.yml -f ./docker/docker-compose.test.yml down
docker-compose -f ./docker/docker-compose.yml -f ./docker/docker-compose.test.yml up -d

cp build/*.make.yml drupal8/
docker-compose -f ./docker/docker-compose.yml -f ./docker/docker-compose.test.yml exec php drush make profile.make.yml --prepare-install --overwrite -y
rm drupal8/*.make.yml

docker-compose -f ./docker/docker-compose.yml -f ./docker/docker-compose.test.yml exec php drush si --db-url=mysql://d8:d8@mariadb/d8 --account-pass=admin -y
docker-compose -f ./docker/docker-compose.yml -f ./docker/docker-compose.test.yml exec php drush en default_content -y

docker-compose -f ./docker/docker-compose.yml -f ./docker/docker-compose.test.yml down
docker-compose -f ./docker/docker-compose.yml -f ./docker/docker-compose.test.yml up -d
docker-compose -f ./docker/docker-compose.yml -f ./docker/docker-compose.test.yml down
