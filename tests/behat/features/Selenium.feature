@javascript
Feature: Simple API test
  In order to check Selenium Standalone Server
  As a QA engineer
  I need to be able to run a simple test using Selenium Standalone Server

  Scenario: Test Success
    Given I am on the homepage
    When I follow "Log in"
    Then I should see "Enter your Site-Install username."
    And I should see "Enter the password that accompanies your username."
