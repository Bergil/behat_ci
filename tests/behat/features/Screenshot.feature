@javascript
Feature: Simple screenshot test
  In order to check screenshots on failed steps
  As a QA engineer
  I need to be able to run a simple test that causes an error

  Scenario: Failed test
    Given I am on the homepage
    When I follow "Log in"
    Then I should see "SOME_LONG_TEXT_THAT_IS_ABSENT_ON THE_PAGE"
